## PicoTest

This repository contains code for various RP2040 based dev boards like the [Pi Pico](https://www.raspberrypi.com/documentation/microcontrollers/raspberry-pi-pico.html) using the [Embassy](https://embassy.dev/) framework.
I am using it as a playground to test some things for other repositories, to keep up to date on the latest changes into Embassy and to have code ready for various modules.

## Setup

### Rust + Tools
1. Install [Rust](https://www.rust-lang.org/tools/install)
2. Install [probe-run](https://crates.io/crates/probe-run) via `cargo install probe-run` for debugging via PicoProbe
3. Install [elf2uf2-rs](https://crates.io/crates/elf2uf2-rs) via `cargo install elf2uf2-rs` for flashing via USB

### Env File
Create a file called `.env` in the root with your secrets.
```
WIFI_SSID=wifinetwork
WIFI_PSK=psk
```

## Running
Run and debug via PicoProbe...
```
cargo run --release --bin mqtt
```
or build and flash via USB...
```
cargo build --release --bin mini_board_test
elf2uf2-rs -d .\target\thumbv6m-none-eabi\release\mini_board_test
```
or setup for running in the VS Code Wokwi Simulator extension...
```
./wokwi/run.ps1 wokwi_demo
```

## Additional things

### Wifi Driver Setup (Pico W)
The driver can be preflashed to the chip and referenced in code (as done in a few bins).
```
cd firmware
probe-rs-cli download 43439A0.bin --format bin --chip RP2040 --base-address 0x10100000
probe-rs-cli download 43439A0_clm.bin --format bin --chip RP2040 --base-address 0x10140000
```
