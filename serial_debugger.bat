echo off
set file=%1

echo off
echo "Flashing..."
elf2uf2-rs -d %file%
if %errorlevel% neq 0 exit /b %errorlevel%

echo "Debugging..."
plink -load CH340 | defmt-print -e %file%
if %errorlevel% neq 0 exit /b %errorlevel%
