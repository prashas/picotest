#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]
#![feature(async_fn_in_trait)]
#![allow(incomplete_features)]

use defmt::*;
use embassy_executor::Spawner;
use embassy_futures::select::select;
use embassy_rp::{gpio, uart};
use embassy_time::{Duration, Timer};
use {defmt_rtt as _, panic_probe as _};

#[embassy_executor::main]
async fn main(_spawner: Spawner) {
  info!("Mosfet Test");

  let p = embassy_rp::init(Default::default());

  let mut serial = uart::Uart::new_blocking(p.UART0, p.PIN_0, p.PIN_1, uart::Config::default());

  serial.blocking_write(b"Hello!\n").unwrap();

  let mut pir1 = gpio::Input::new(p.PIN_18, gpio::Pull::Down);
  let mut pir2 = gpio::Input::new(p.PIN_19, gpio::Pull::Down);

  let mut led = gpio::Output::new(p.PIN_20, gpio::Level::Low);

  loop {
    serial.blocking_write(b"Waiting for motion...\n").unwrap();
    select(pir1.wait_for_rising_edge(), pir2.wait_for_rising_edge()).await;

    serial.blocking_write(b"Motion detected!!!\n").unwrap();
    led.set_high();

    Timer::after(Duration::from_secs(5)).await;
    led.set_low();
  }
}
