#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]
#![feature(async_fn_in_trait)]
#![allow(incomplete_features)]

use defmt::*;
use embassy_executor::Spawner;
use embassy_rp::adc::{Adc, Channel, Config, InterruptHandler};
use embassy_rp::peripherals::PIO0;
use embassy_rp::pio::Pio;
use embassy_rp::{bind_interrupts, gpio};
use embassy_time::{Duration, Timer};
use picotest::ws2812;
use {defmt_rtt as _, panic_probe as _};

bind_interrupts!(struct Irqs {
  PIO0_IRQ_0 => embassy_rp::pio::InterruptHandler<PIO0>;
  ADC_IRQ_FIFO => InterruptHandler;
});

#[embassy_executor::main]
async fn main(_spawner: Spawner) {
  let p = embassy_rp::init(Default::default());

  let _power = gpio::Output::new(p.PIN_14, gpio::Level::High);
  let mut adc = Adc::new(p.ADC, Irqs, Config::default());
  let mut p26 = Channel::new_pin(p.PIN_26, gpio::Pull::None);

  let mut pio0 = Pio::new(p.PIO0, Irqs);
  let mut led = ws2812::Ws2812::new(p.PIN_23, &mut pio0.common, pio0.sm1);
  led.set_color(&ws2812::RGB { r: 0, g: 0, b: 0 }).await;

  loop {
    let level = adc.read(&mut p26).await.unwrap();
    info!("Pin 26 ADC: {}", level);

    let level_percent = (level as f32) / 3800.0;
    let redness = 20 + (level_percent * 235.0) as u8;

    led.set_color(&ws2812::RGB { r: redness, g: 0, b: 0 }).await;

    Timer::after(Duration::from_millis(50)).await;
  }
}
