#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]
#![feature(async_fn_in_trait)]
#![allow(incomplete_features)]

use defmt::*;
use embassy_executor::Spawner;
use embassy_rp::gpio::{Input, Pin, Pull};
use embassy_rp::peripherals::UART1;
use embassy_rp::uart::{Config, InterruptHandler, Parity, StopBits, Uart};
use embassy_rp::{bind_interrupts, clocks, pac};
use embassy_time::{Duration, Instant, Timer};
use heapless::String;
use picotest::jdy::Jdy24mMaster;
use {defmt_rtt as _, panic_probe as _};

const FLASH_THRESHOLD: u64 = 750;
const TARGET_MAC: &str = "8C884B2558EC";

bind_interrupts!(struct Irqs {
  UART1_IRQ => InterruptHandler<UART1>;
});

#[embassy_executor::main]
async unsafe fn main(_spawner: Spawner) {
  let crystal_hz = 12_000_000;
  let mut cc = clocks::ClockConfig::crystal(crystal_hz);
  cc.xosc = Some(clocks::XoscConfig {
    hz: crystal_hz,
    sys_pll: None,
    usb_pll: None,
    delay_multiplier: 128,
  });
  cc.ref_clk = clocks::RefClkConfig {
    src: clocks::RefClkSrc::Xosc,
    div: 4,
  };
  cc.sys_clk = clocks::SysClkConfig {
    src: clocks::SysClkSrc::Xosc,
    div_int: 4,
    div_frac: 0,
  };
  cc.peri_clk_src = Some(clocks::PeriClkSrc::Xosc);
  cc.rosc = None;
  cc.adc_clk = None;
  cc.usb_clk = None;
  cc.rtc_clk = None;

  let config = embassy_rp::config::Config::new(cc);

  let p = embassy_rp::init(config);

  pac::PLL_SYS.pwr().modify(|w| {
    w.set_pd(true);
  });
  pac::PLL_USB.pwr().modify(|w| {
    w.set_pd(true);
  });
  pac::ROSC.ctrl().modify(|w| {
    w.set_enable(pac::rosc::vals::Enable::DISABLE);
  });

  let mut button = Input::new(p.PIN_1, Pull::Up);

  let mut uart_config = Config::default();
  uart_config.baudrate = 115200;
  uart_config.stop_bits = StopBits::STOP1;
  uart_config.parity = Parity::ParityNone;
  let uart = Uart::new(p.UART1, p.PIN_4, p.PIN_5, Irqs, p.DMA_CH0, p.DMA_CH1, uart_config);

  let mut jdy = Jdy24mMaster::new(uart, p.PIN_8.degrade());

  jdy.init().await.unwrap();

  jdy.connect_and_send(TARGET_MAC, "powermeter,usage=0").await.unwrap();

  let mut last_sent_watt_value: i32 = 0;
  loop {
    button.wait_for_high().await;
    let epoch_now = Instant::now().as_millis();
    Timer::after(Duration::from_millis(FLASH_THRESHOLD)).await;
    button.wait_for_low().await;

    let epoch_diff = Instant::now().as_millis() - epoch_now;
    info!("Epoch diff? {}", epoch_diff);

    let new_watt_value = 3600000 / epoch_diff as i32;
    info!("Measure: {}wH", new_watt_value);

    if (new_watt_value - last_sent_watt_value).abs() < 200 {
      Timer::after(Duration::from_millis(FLASH_THRESHOLD)).await;
      continue;
    }

    let mut data: String<50> = String::new();
    core::fmt::write(&mut data, format_args!("powermeter,usage={}", new_watt_value)).unwrap();

    if jdy.connect_and_send(TARGET_MAC, &data).await.is_ok() {
      last_sent_watt_value = new_watt_value;
    } else {
      error!("Failed to send data");
    }
  }
}
