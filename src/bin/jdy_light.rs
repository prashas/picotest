#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]
#![feature(async_fn_in_trait)]
#![allow(incomplete_features)]

use core::fmt::Arguments;

use defmt::*;
use embassy_executor::Spawner;
use embassy_rp::bind_interrupts;
use embassy_rp::peripherals::{PIO0, UART1};
use embassy_rp::pio::Pio;
use embassy_rp::uart::{Config, Parity, StopBits, Uart};
use embassy_sync::blocking_mutex::raw::CriticalSectionRawMutex;
use embassy_sync::signal::Signal;
use embassy_time::{with_timeout, Duration, Timer};
use picotest::jdy::Jdy;
use picotest::ws2812;
use {defmt_rtt as _, panic_probe as _};

bind_interrupts!(struct IrqsPio {
  PIO0_IRQ_0 => embassy_rp::pio::InterruptHandler<PIO0>;
});

bind_interrupts!(struct IrqsUart {
  UART1_IRQ => embassy_rp::uart::InterruptHandler<UART1>;
});

static LED_SIGNAL: Signal<CriticalSectionRawMutex, ws2812::RGB> = Signal::new();

#[embassy_executor::task]
async fn set_led_color(mut led: ws2812::Ws2812<'static, PIO0, 1>) {
  led.set_color(&ws2812::RGB { r: 11, g: 11, b: 11 }).await;
  loop {
    let color = LED_SIGNAL.wait().await;
    led.set_color(&color).await;
  }
}

#[embassy_executor::main]
async fn main(spawner: Spawner) {
  let p = embassy_rp::init(Default::default());

  let mut pio0 = Pio::new(p.PIO0, IrqsPio);

  let led = ws2812::Ws2812::new(p.PIN_23, &mut pio0.common, pio0.sm1);
  spawner.spawn(set_led_color(led)).unwrap();

  let mut uart_config = Config::default();
  uart_config.baudrate = 9600;
  uart_config.stop_bits = StopBits::STOP1;
  uart_config.parity = Parity::ParityNone;
  let uart = Uart::new(p.UART1, p.PIN_4, p.PIN_5, IrqsUart, p.DMA_CH0, p.DMA_CH1, uart_config);

  Timer::after(Duration::from_secs(1)).await;
  let mut jdy = Jdy::new(uart);

  info!("Ready to go");

  async fn send_instruction_and_confirm_ok<'a>(jdy: &mut Jdy<UART1>, command: Arguments<'a>) -> Result<(), ()> {
    jdy.send_with_args(format_args!("AT+{}\r\n", command)).await?;
    with_timeout(Duration::from_millis(5000), jdy.wait_for_data()).await.map_err(|_| ())??;
    Ok(())
  }

  send_instruction_and_confirm_ok(&mut jdy, format_args!("ROLE2")).await.unwrap();
  send_instruction_and_confirm_ok(&mut jdy, format_args!("NAMBBluey123")).await.unwrap();
  send_instruction_and_confirm_ok(&mut jdy, format_args!("RESET")).await.unwrap();

  loop {
    let data = jdy.wait_for_data().await;
    if data.is_err() {
      continue;
    }

    let data = data.unwrap();
    if data.starts_with('#') {
      match ws2812::RGB::from_hex_string(data.get(1..).unwrap()) {
        Ok(color) => {
          info!("Got a color: {}", data);
          LED_SIGNAL.signal(color);
        }
        Err(_) => {
          info!("Could not parse color {}", data);
        }
      }
    }

    match jdy.send_with_args(format_args!("Testing!!")).await {
      Ok(_) => info!("Sent the message!"),
      Err(e) => error!("Failed to send {:?}", e),
    }

    Timer::after(Duration::from_secs(1)).await;
  }
}
