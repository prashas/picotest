#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]
#![feature(async_fn_in_trait)]
#![allow(incomplete_features)]

use defmt::*;
use embassy_executor::Spawner;
use embassy_futures::join::join;
use embassy_futures::select;
use embassy_rp::gpio::Pin;
use embassy_rp::peripherals::UART1;
use embassy_rp::{bind_interrupts, clocks, gpio, pac, uart};
use embassy_time::{Duration, Timer};
use picotest::jdy::Jdy24mMaster;
use {defmt_serial as _, panic_probe as _};

const TARGET_MAC: &str = "8C884B2558EC";

const POWER_SAVE_DIV: u32 = 256;
const POWER_SAVE_CLOCK_FACTOR: u64 = 60;

bind_interrupts!(struct Irqs {
  UART1_IRQ => uart::InterruptHandler<UART1>;
});

#[embassy_executor::main]
async fn main(_spawner: Spawner) {
  let crystal_hz = 12_000_000;
  let mut cc = clocks::ClockConfig::crystal(crystal_hz);
  cc.xosc = Some(clocks::XoscConfig {
    hz: crystal_hz,
    sys_pll: Some(clocks::PllConfig {
      refdiv: 1,
      fbdiv: 125,
      post_div1: 6,
      post_div2: 2,
    }),
    usb_pll: None,
    delay_multiplier: 128,
  });
  cc.ref_clk = clocks::RefClkConfig {
    src: clocks::RefClkSrc::Xosc,
    div: 4,
  };
  cc.sys_clk = clocks::SysClkConfig {
    src: clocks::SysClkSrc::Xosc,
    div_int: 1,
    div_frac: 0,
  };
  cc.peri_clk_src = Some(clocks::PeriClkSrc::Xosc);
  cc.rosc = None;
  cc.adc_clk = None;
  cc.usb_clk = None;

  cc.rtc_clk = None;

  let config = embassy_rp::config::Config::new(cc);

  let p = embassy_rp::init(config);

  pac::PLL_SYS.pwr().modify(|w| {
    w.set_pd(true);
  });
  pac::PLL_USB.pwr().modify(|w| {
    w.set_pd(true);
  });
  pac::ROSC.ctrl().modify(|w| {
    w.set_enable(pac::rosc::vals::Enable::DISABLE);
  });

  // let mut log_uart = uart::Uart::new_blocking(p.UART0, p.PIN_12, p.PIN_13, uart::Config::default());
  // defmt_serial::defmt_serial(log_uart);

  info!("Starting...");

  let uart = uart::Uart::new(p.UART1, p.PIN_4, p.PIN_5, Irqs, p.DMA_CH0, p.DMA_CH1, uart::Config::default());

  let mut jdy = Jdy24mMaster::new(uart, p.PIN_8.degrade());

  let _ = jdy.init().await;

  info!("Ready to go");

  let mut letter_trigger = gpio::Input::new(p.PIN_2, gpio::Pull::Up);
  let mut door_trigger = gpio::Input::new(p.PIN_3, gpio::Pull::Up);

  let mut has_letters = false;

  loop {
    info!("Sleep till next event");
    set_clocks_to_power_save(true);
    let message = match select::select3(
      letter_trigger.wait_for_rising_edge(),
      door_trigger.wait_for_rising_edge(),
      Timer::after(Duration::from_secs(300 / POWER_SAVE_CLOCK_FACTOR)),
    )
    .await
    {
      select::Either3::First(_) => {
        info!("Letter trigger");
        has_letters = true;
        "letterbox,signal=1"
      }
      select::Either3::Second(_) => {
        info!("Door trigger");
        has_letters = false;
        "letterbox,signal=2"
      }
      select::Either3::Third(_) => {
        info!("Timer trigger");
        if has_letters {
          "letterbox,signal=1"
        } else {
          "letterbox,signal=0"
        }
      }
    };
    set_clocks_to_power_save(false);
    let _ = jdy.connect_and_send(TARGET_MAC, message).await;
    info!("Waiting for low");
    set_clocks_to_power_save(true);
    join(letter_trigger.wait_for_low(), door_trigger.wait_for_low()).await;
  }
}

fn set_clocks_to_power_save(power_save: bool) {
  pac::CLOCKS.clk_sys_div().write(|w| {
    w.set_int(if power_save { POWER_SAVE_DIV } else { 1 });
    w.set_frac(0);
  });
}
