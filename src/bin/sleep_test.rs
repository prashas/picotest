#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]
#![feature(async_fn_in_trait)]
#![allow(incomplete_features)]

use defmt::*;
use embassy_executor::Spawner;
use embassy_rp::gpio::{Input, InterruptTrigger, Level, Output, Pull};
use embassy_rp::{clocks, pac};
use embassy_time::{Duration, Timer};
use picotest::sleep;
use {defmt_rtt as _, panic_probe as _};

#[embassy_executor::main]
async unsafe fn main(_spawner: Spawner) {
  let crystal_hz = 12_000_000;
  let mut cc = clocks::ClockConfig::crystal(crystal_hz);
  cc.xosc = Some(clocks::XoscConfig {
    hz: crystal_hz,
    sys_pll: None,
    usb_pll: None,
    delay_multiplier: 128,
  });
  cc.ref_clk = clocks::RefClkConfig {
    src: clocks::RefClkSrc::Xosc,
    div: 4,
  };
  cc.sys_clk = clocks::SysClkConfig {
    src: clocks::SysClkSrc::Xosc,
    div_int: 4,
    div_frac: 0,
  };
  cc.peri_clk_src = Some(clocks::PeriClkSrc::Xosc);
  cc.rosc = None;
  cc.adc_clk = None;
  cc.usb_clk = None;
  cc.rtc_clk = None;

  let config = embassy_rp::config::Config::new(cc);

  let p = embassy_rp::init(config);

  pac::PLL_SYS.pwr().modify(|w| {
    w.set_pd(true);
  });
  pac::PLL_USB.pwr().modify(|w| {
    w.set_pd(true);
  });
  pac::ROSC.ctrl().modify(|w| {
    w.set_enable(pac::rosc::vals::Enable::DISABLE);
  });

  let mut _button = Input::new(p.PIN_20, Pull::Up);
  let mut light = Output::new(p.PIN_25, Level::Low);

  info!("Lets go!");

  loop {
    info!("start of loop");
    sleep::go_dormant_until_pin(20, InterruptTrigger::LevelLow, sleep::DormantSource::XOSC);
    info!("Triggering light");
    light.set_high();
    Timer::after(Duration::from_secs(2)).await;
    light.set_low();
    info!("end of loop");
  }
}
