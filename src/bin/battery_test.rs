#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]
#![feature(async_fn_in_trait)]
#![allow(incomplete_features)]

use defmt::*;
use embassy_executor::Spawner;
use embassy_rp::{clocks, gpio, pac};
use embassy_time::{Duration, Timer};
use {defmt_rtt as _, panic_probe as _};

#[embassy_executor::main]
async fn main(_spawner: Spawner) {
  let crystal_hz = 12_000_000;
  let mut cc = clocks::ClockConfig::crystal(crystal_hz);
  cc.xosc = Some(clocks::XoscConfig {
    hz: crystal_hz,
    sys_pll: Some(clocks::PllConfig {
      refdiv: 1,
      fbdiv: 125,
      post_div1: 6,
      post_div2: 2,
    }),
    usb_pll: None,
    delay_multiplier: 128,
  });
  cc.ref_clk = clocks::RefClkConfig {
    src: clocks::RefClkSrc::Xosc,
    div: 4,
  };
  cc.sys_clk = clocks::SysClkConfig {
    src: clocks::SysClkSrc::Xosc,
    div_int: 1,
    div_frac: 0,
  };
  cc.peri_clk_src = Some(clocks::PeriClkSrc::Xosc);
  cc.rosc = None;
  cc.adc_clk = None;
  cc.usb_clk = None;
  cc.rtc_clk = None;

  let config = embassy_rp::config::Config::new(cc);

  let p = embassy_rp::init(config);

  pac::PLL_SYS.pwr().modify(|w| {
    w.set_pd(true);
  });
  pac::PLL_USB.pwr().modify(|w| {
    w.set_pd(true);
  });
  pac::ROSC.ctrl().modify(|w| {
    w.set_enable(pac::rosc::vals::Enable::DISABLE);
  });

  info!("Ready to go");

  let mut led = gpio::Output::new(p.PIN_16, gpio::Level::Low);

  async fn wait_for_duration_in_low_power(duration_secs: u64) {
    info!("Set clock");
    pac::CLOCKS.clk_sys_div().write(|w| {
      w.set_int(256);
      w.set_frac(0);
    });
    info!("Waiting for signal");
    Timer::after(Duration::from_millis((duration_secs * 1000) / 64)).await;
    pac::CLOCKS.clk_sys_div().write(|w| {
      w.set_int(1);
      w.set_frac(0);
    });
  }

  loop {
    led.set_low();
    wait_for_duration_in_low_power(2).await;
    led.set_high();
    Timer::after(Duration::from_secs(1)).await;
    info!("Looping again...");
  }
}
