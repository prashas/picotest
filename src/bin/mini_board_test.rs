#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]
#![feature(async_fn_in_trait)]
#![allow(incomplete_features)]

use defmt::*;
use embassy_executor::Spawner;
use embassy_rp::{bind_interrupts, peripherals, pio, uart};
use embassy_time::{Duration, Timer};
use panic_probe as _;
use picotest::ws2812;

bind_interrupts!(struct Irqs {
  PIO0_IRQ_0 => pio::InterruptHandler<peripherals::PIO0>;
});

#[embassy_executor::main]
async fn main(_spawner: Spawner) {
  let p = embassy_rp::init(Default::default());

  let uart = uart::Uart::new_blocking(p.UART0, p.PIN_12, p.PIN_13, uart::Config::default());
  defmt_serial::defmt_serial(uart);
  info!("Welcome!");

  let mut pio0 = pio::Pio::new(p.PIO0, Irqs);
  let mut led = ws2812::Ws2812::new(p.PIN_16, &mut pio0.common, pio0.sm1);

  let colors = [
    ws2812::RGB { r: 100, g: 0, b: 0 },
    ws2812::RGB { r: 50, g: 50, b: 0 },
    ws2812::RGB { r: 0, g: 100, b: 0 },
    ws2812::RGB { r: 0, g: 50, b: 50 },
    ws2812::RGB { r: 0, g: 0, b: 100 },
    ws2812::RGB { r: 50, g: 0, b: 50 },
  ];

  let mut idx = 0;
  loop {
    info!("Loopy!");
    led.set_color(&colors[idx]).await;
    Timer::after(Duration::from_millis(500)).await;
    idx = (idx + 1) % colors.len();
  }
}
