#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]
#![feature(async_fn_in_trait)]
#![allow(incomplete_features)]

use defmt::*;
use embassy_executor::Spawner;
use embassy_rp::rtc::{DateTime, DayOfWeek, Rtc};
use embassy_time::{Duration, Timer};
use {defmt_rtt as _, panic_probe as _};

#[embassy_executor::main]
async fn main(_spawner: Spawner) {
  let p = embassy_rp::init(Default::default());

  let mut rtc = Rtc::new(p.RTC);
  rtc
    .set_datetime(DateTime {
      year: 2023,
      month: 1,
      day: 1,
      day_of_week: DayOfWeek::Monday,
      hour: 0,
      minute: 0,
      second: 0,
    })
    .expect("Failed to init RTC");

  info!("Is the RTC running? {:?}", rtc.is_running());

  loop {
    info!("RTC seconds is {:?}", rtc.now().unwrap().second);
    Timer::after(Duration::from_secs(1)).await;
  }
}
