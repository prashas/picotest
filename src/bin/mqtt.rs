#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]
#![feature(async_fn_in_trait)]
#![allow(incomplete_features)]

use defmt::*;
use embassy_executor::Spawner;
use embassy_rp::{bind_interrupts, gpio, peripherals, pio};
use embassy_sync::blocking_mutex::raw::CriticalSectionRawMutex;
use embassy_sync::signal::Signal;
use embassy_time::{Duration, Timer};
use heapless::String;
use {defmt_rtt as _, panic_probe as _};

bind_interrupts!(struct Irqs {
  PIO0_IRQ_0 => pio::InterruptHandler<peripherals::PIO0>;
});

#[embassy_executor::task]
async fn handle_rx(rx_stream: &'static Signal<CriticalSectionRawMutex, (String<100>, String<500>)>, pin: peripherals::PIN_18) -> ! {
  let mut output = gpio::Output::new(pin, gpio::Level::Low);
  loop {
    let (topic, payload) = rx_stream.wait().await;
    match topic.as_str() {
      "picotest/control" => {
        info!("Got control message: {}", payload);
        if payload == "on" {
          output.set_high();
        } else if payload == "off" {
          output.set_low();
        } else {
          warn!("Unknown payload {}", payload);
        }
      }
      _ => {
        warn!("Unknown topic {}", topic);
      }
    }
  }
}

#[embassy_executor::main]
async fn main(spawner: Spawner) {
  let p = embassy_rp::init(Default::default());

  let (rx, tx) = picotest::mqtt::on_pico_w(
    Irqs,
    picotest::mqtt::WifiResources {
      pwr: p.PIN_23,
      dio: p.PIN_24,
      cs: p.PIN_25,
      clk: p.PIN_29,
      pio: p.PIO0,
      dma: p.DMA_CH0,
    },
    &spawner,
    "picotest",
  )
  .await;

  unwrap!(spawner.spawn(handle_rx(rx, p.PIN_18)));

  loop {
    info!("Looping...");
    tx.signal((String::from("picotest/status"), String::from("Test")));
    Timer::after(Duration::from_secs(5)).await;
  }
}
