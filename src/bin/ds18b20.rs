#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]
#![feature(async_fn_in_trait)]
#![allow(incomplete_features)]

use defmt::*;
use embassy_executor::Spawner;
use embassy_rp::gpio::Pin;
use embassy_time::{Duration, Timer};
use picotest::one_wire::OneWire;
use {defmt_rtt as _, panic_probe as _};

// https://www.analog.com/media/en/technical-documentation/data-sheets/DS18B20.pdf
async fn get_temp_result<'a>(device: &mut OneWire<'a>) -> Option<f32> {
  let is_present = device.reset().await;
  if !is_present {
    error!("Device not present");
    return None;
  }

  device.write_byte(0xCC).await;
  device.write_byte(0x44).await;

  while !(device.read_bit().await) {}

  device.reset().await;
  device.write_byte(0xCC).await;
  device.write_byte(0xBE).await;

  let temperature_lsb = device.read_byte().await;
  let temperature_msb = device.read_byte().await;

  let temperature_reading = ((temperature_msb as u16) << 8) | (temperature_lsb as u16);
  Some((temperature_reading as f32) / 16.00)
}

#[embassy_executor::main]
async fn main(_spawner: Spawner) {
  let p = embassy_rp::init(Default::default());
  info!("Starting");
  let mut onewire = OneWire::new(p.PIN_9.degrade());
  loop {
    let temp = get_temp_result(&mut onewire).await;
    info!("Temp? {}", temp);
    Timer::after(Duration::from_millis(500)).await;
  }
}
