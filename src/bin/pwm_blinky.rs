#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]
#![feature(async_fn_in_trait)]
#![allow(incomplete_features)]

use defmt::*;
use embassy_executor::Spawner;
use embassy_rp::pwm::{Config, Pwm};
use embassy_time::{Duration, Timer};
use {defmt_rtt as _, panic_probe as _};

#[embassy_executor::main]
async fn main(_spawner: Spawner) {
  info!("Mosfet Test");

  let p = embassy_rp::init(Default::default());

  let mut c: Config = Default::default();
  c.top = 0x8000;
  c.compare_a = 8;
  let mut pwm = Pwm::new_output_a(p.PWM_CH2, p.PIN_20, c.clone());

  let mut is_going_up = true;
  loop {
    info!("current LED duty cycle: {}/32768", c.compare_a);
    Timer::after(Duration::from_millis(50)).await;
    if c.compare_a > 30000 || c.compare_a == 1 {
      is_going_up = !is_going_up;
    }
    if is_going_up {
      c.compare_a = c.compare_a.rotate_left(1);
    } else {
      c.compare_a = c.compare_a.rotate_right(1);
    }
    pwm.set_config(&c);
  }
}
