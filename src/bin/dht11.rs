#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]
#![feature(async_fn_in_trait)]
#![allow(incomplete_features)]

use defmt::*;
use embassy_executor::Spawner;
use embassy_rp::gpio::{AnyPin, Flex, Pin, Pull};
use embassy_time::{Duration, Instant, Timer};
use {defmt_rtt as _, panic_probe as _};

async fn time_transition(dht: &mut Flex<'static, AnyPin>) -> u64 {
  let start_time = Instant::now();
  dht.wait_for_any_edge().await;
  let end_time = Instant::now();
  end_time.duration_since(start_time).as_micros()
}

#[embassy_executor::main]
async fn main(_spawner: Spawner) {
  let p = embassy_rp::init(Default::default());

  let mut dht = Flex::new(p.PIN_9.degrade());
  dht.set_pull(Pull::Up);

  loop {
    // https://www.mouser.com/datasheet/2/758/DHT11-Technical-Data-Sheet-Translated-Version-1143054.pdf
    dht.set_as_output();
    dht.set_high();
    Timer::after(Duration::from_millis(25)).await;

    dht.set_low();
    Timer::after(Duration::from_millis(20)).await;

    dht.set_as_input();

    dht.wait_for_low().await;
    let low_time = time_transition(&mut dht).await;
    let high_time = time_transition(&mut dht).await;
    if low_time < 50 || high_time < 50 {
      error!("Bad handshake times L:{}, H:{}", low_time, high_time);
      continue;
    }

    let mut bits = [0u8; 40];
    for bit in &mut bits {
      dht.wait_for_high().await;
      let time_taken = time_transition(&mut dht).await;
      *bit = if time_taken > 50 { 1 } else { 0 };
    }

    let humidity = bits[0..8].iter().fold(0, |acc, &b| acc * 2 + b);
    let temperature = bits[16..24].iter().fold(0, |acc, &b| acc * 2 + b);

    // TODO: Checksum...

    info!("Temp {}, Humidity {}", temperature, humidity);
  }
}
