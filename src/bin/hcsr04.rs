#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]
#![feature(async_fn_in_trait)]
#![allow(incomplete_features)]

use defmt::*;
use embassy_executor::Spawner;
use embassy_rp::gpio::{AnyPin, Input, Level, Output, Pin, Pull};
use embassy_time::{with_timeout, Duration, Instant, Timer};
use {defmt_rtt as _, panic_probe as _};

async fn measure_distance(echo: &mut Input<'_, AnyPin>, trigger: &mut Output<'_, AnyPin>) -> Option<u64> {
  trigger.set_high();
  Timer::after(Duration::from_micros(10)).await;
  trigger.set_low();

  if with_timeout(Duration::from_micros(1000), echo.wait_for_high()).await.is_err() {
    return None;
  }
  let start_time = Instant::now();
  if with_timeout(Duration::from_micros(58 * 400), echo.wait_for_low()).await.is_err() {
    return None;
  }
  let duration = Instant::now().duration_since(start_time);
  let distance = duration.as_micros() / 58;
  Some(distance)
}

#[embassy_executor::main]
async fn main(_spawner: Spawner) {
  let p = embassy_rp::init(Default::default());

  let mut echo = Input::new(p.PIN_10.degrade(), Pull::Down);
  let mut trigger = Output::new(p.PIN_11.degrade(), Level::Low);

  loop {
    let distance = measure_distance(&mut echo, &mut trigger).await;
    info!("Distance: {}", distance);
    Timer::after(Duration::from_millis(60)).await;
  }
}
