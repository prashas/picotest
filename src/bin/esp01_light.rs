#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]
#![feature(async_fn_in_trait)]
#![allow(incomplete_features)]

use defmt::*;
use embassy_executor::Spawner;
use embassy_rp::bind_interrupts;
use embassy_rp::gpio::{Level, Output};
use embassy_rp::peripherals::{PIO0, UART1};
use embassy_rp::pio::Pio;
use embassy_rp::uart::{Config, Uart};
use embassy_sync::blocking_mutex::raw::CriticalSectionRawMutex;
use embassy_sync::signal::Signal;
use embassy_time::{Duration, Timer};
use picotest::esp01::Esp01;
use picotest::ws2812;
use rust_mqtt::client::client::MqttClient;
use rust_mqtt::client::client_config::ClientConfig;
use rust_mqtt::utils::rng_generator::CountingRng;
use static_cell::make_static;
use {defmt_rtt as _, panic_probe as _};

bind_interrupts!(struct Irqs {
  PIO0_IRQ_0 => embassy_rp::pio::InterruptHandler<PIO0>;
  UART1_IRQ => embassy_rp::uart::InterruptHandler<UART1>;
});

static LED_SIGNAL: Signal<CriticalSectionRawMutex, ws2812::RGB> = Signal::new();

#[embassy_executor::task]
async fn process_messages(client: &'static mut MqttClient<'static, &'static mut Esp01, 5, CountingRng>) {
  client.subscribe_to_topic("picotest/color").await.unwrap();
  loop {
    match client.receive_message().await {
      Ok((topic, payload)) => {
        info!("MQTT mesage inc: {} {}", topic, payload.len());
        #[allow(clippy::single_match)]
        match topic {
          "picotest/color" => {
            let payload_str = core::str::from_utf8(payload);
            if payload_str.is_err() {
              warn!("Failed to parse payload as utf8");
              continue;
            }
            let payload_str = payload_str.unwrap();
            debug!("Payload: {}", payload_str);
            match ws2812::RGB::from_hex_string(payload_str.trim_start_matches('#')) {
              Ok(color) => {
                LED_SIGNAL.signal(color);
              }
              Err(_) => {
                warn!("Could not parse color {}", payload_str);
              }
            }
          }
          _ => {}
        }
      }
      Err(e) => {
        warn!("Failed to receive message: {:?}", e);
      }
    }
  }
}

#[embassy_executor::task]
async fn handle_color(mut led: ws2812::Ws2812<'static, PIO0, 1>) {
  loop {
    let color = LED_SIGNAL.wait().await;
    led.set_color(&color).await;
  }
}

#[embassy_executor::main]
async fn main(spawner: Spawner) {
  let p = embassy_rp::init(Default::default());

  let mut pio0 = Pio::new(p.PIO0, Irqs);
  let led = ws2812::Ws2812::new(p.PIN_23, &mut pio0.common, pio0.sm1);

  unwrap!(spawner.spawn(handle_color(led)));

  let mut esp_control = Output::new(p.PIN_20, Level::Low);

  let uart = Uart::new(p.UART1, p.PIN_4, p.PIN_5, Irqs, p.DMA_CH0, p.DMA_CH1, Config::default());

  esp_control.set_high();
  let esp = make_static!(Esp01::new(uart));

  esp.drain_until_timeout(Duration::from_secs(1)).await;

  esp.command(format_args!("ATE0"), &["OK"]).await.unwrap();

  esp.command(format_args!("AT+CWMODE_CUR=1"), &["OK"]).await.unwrap();

  info!("Lets connect....");
  esp
    .command(
      format_args!("AT+CWJAP_CUR=\"{}\",\"{}\"", env!("WIFI_SSID"), env!("WIFI_PSK")),
      &["OK", "FAIL"],
    )
    .await
    .unwrap();

  info!("connect to FIDO....");
  esp
    .command(
      format_args!("AT+CIPSTART=\"TCP\",\"192.168.1.4\",1883"),
      &["OK", "ERROR", "ALREADY CONNECT"],
    )
    .await
    .unwrap();

  esp.start_transparent_transmission().await.unwrap();

  info!("==================== MQTT BLOCK START ==================");

  let mut config = ClientConfig::new(rust_mqtt::client::client_config::MqttVersion::MQTTv5, CountingRng(20000));
  config.add_max_subscribe_qos(rust_mqtt::packet::v5::publish_packet::QualityOfService::QoS1);
  config.add_client_id("pico");
  config.keep_alive = 0;

  let recv_buffer = make_static!([0; 80]);
  let write_buffer = make_static!([0; 80]);
  let client = make_static!(MqttClient::<_, 5, _>::new(esp, write_buffer, 80, recv_buffer, 80, config));

  if let Err(e) = client.connect_to_broker().await {
    loop {
      warn!("Failed to connect to broker: {:?}", e);
      Timer::after(Duration::from_secs(1)).await;
    }
  } else {
    info!("=============== CONNECTED TO BROKER! ===============")
  }

  if let Err(e) = client
    .send_message(
      "picotest",
      b"omg esp01",
      rust_mqtt::packet::v5::publish_packet::QualityOfService::QoS1,
      false,
    )
    .await
  {
    loop {
      warn!("Failed to send message: {:?}", e);
      Timer::after(Duration::from_secs(1)).await;
    }
  } else {
    info!("=============== SENT MESSAGE! ===============")
  }

  unwrap!(spawner.spawn(process_messages(client)));

  loop {
    debug!("Main thread yo!");
    Timer::after(Duration::from_secs(5)).await;
  }
}
