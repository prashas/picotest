#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]
#![feature(async_fn_in_trait)]
#![allow(incomplete_features)]

use defmt::*;
use embassy_executor::Spawner;
use embassy_rp::bind_interrupts;
use embassy_rp::gpio::Pin;
use embassy_rp::peripherals::UART0;
use embassy_rp::uart::{Config, InterruptHandler, Parity, StopBits, Uart};
use embassy_time::{Duration, Timer};
use heapless::String;
use picotest::jdy::Jdy24mMaster;
use {defmt_rtt as _, panic_probe as _};

bind_interrupts!(struct Irqs {
  UART0_IRQ => InterruptHandler<UART0>;
});

#[embassy_executor::main]
async fn main(_spawner: Spawner) {
  let p = embassy_rp::init(Default::default());

  let mut uart_config = Config::default();
  uart_config.baudrate = 115200;
  uart_config.stop_bits = StopBits::STOP1;
  uart_config.parity = Parity::ParityNone;
  let uart = Uart::new(p.UART0, p.PIN_16, p.PIN_17, Irqs, p.DMA_CH0, p.DMA_CH1, uart_config);

  Timer::after(Duration::from_secs(1)).await;
  let mut jdy = Jdy24mMaster::new(uart, p.PIN_18.degrade());

  match jdy.init().await {
    Ok(_) => info!("Finished init!"),
    Err(e) => error!("Failed to init {:?}", e),
  }

  info!("Ready to go");

  let mut collatz = 641298;
  loop {
    collatz = if collatz % 2 == 0 { collatz / 2 } else { 3 * collatz + 1 };
    info!("Collatz: {}", collatz);
    let mut data: String<200> = String::new();
    core::fmt::write(&mut data, format_args!("mydevicename,collatz={},somestring=\"test\"", collatz)).unwrap();

    info!("Start sending");
    // pc bluetooth adapter "001A7DDA7113"
    // homenode "8C884B2558EC"
    match jdy.connect_and_send("8C884B2558EC", &data).await {
      Ok(_) => info!("Send succeeded!"),
      Err(e) => error!("Failed to send data {:?}", e),
    }
    Timer::after(Duration::from_millis(10000)).await;
  }
}
