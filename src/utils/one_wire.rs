use embassy_rp::gpio::{AnyPin, Flex, Pull};
use embassy_time::{Duration, Timer};

pub struct OneWire<'a> {
  pin: Flex<'a, AnyPin>,
}

impl<'a> OneWire<'a> {
  pub fn new(pin: AnyPin) -> Self {
    let mut pin = Flex::new(pin);
    pin.set_pull(Pull::Up);
    Self { pin }
  }

  pub async fn reset(&mut self) -> bool {
    self.pin.set_high();
    self.pin.set_as_output();
    Timer::after(Duration::from_micros(5)).await;

    self.pin.set_low();
    Timer::after(Duration::from_micros(480)).await;

    self.pin.set_as_input();
    Timer::after(Duration::from_micros(70)).await;

    let device_present = self.pin.is_low();

    Timer::after(Duration::from_micros(480)).await;

    device_present
  }

  async fn write_bit(&mut self, is_high: bool) {
    let low_time = if is_high { 6 } else { 60 };
    self.pin.set_low();
    self.pin.set_as_output();
    Timer::after(Duration::from_micros(low_time)).await;
    self.pin.set_as_input();
    Timer::after(Duration::from_micros(70 - low_time)).await;
  }

  pub async fn write_byte(&mut self, mut byte: u8) {
    for _ in 0..8 {
      self.write_bit(byte & 0x01 == 0x01).await;
      byte >>= 1;
    }
  }

  pub async fn read_bit(&mut self) -> bool {
    self.pin.set_low();
    self.pin.set_as_output();
    Timer::after(Duration::from_micros(5)).await;
    self.pin.set_as_input();
    Timer::after(Duration::from_micros(9)).await;
    let is_high = self.pin.is_high();
    Timer::after(Duration::from_micros(55)).await;
    is_high
  }

  /**
   * Reads a byte LSB first
   */
  pub async fn read_byte(&mut self) -> u8 {
    let mut byte = 0u8;
    for i in 0..8 {
      if self.read_bit().await {
        byte |= 1u8 << i;
      }
    }
    byte
  }
}
