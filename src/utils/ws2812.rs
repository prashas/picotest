use embassy_rp::clocks;
use embassy_rp::pio::{Common, Config, FifoJoin, Instance, PioPin, ShiftConfig, ShiftDirection, StateMachine};
use fixed::types::U24F8;
use fixed_macro::fixed;

pub struct RGB {
  pub r: u8,
  pub g: u8,
  pub b: u8,
}

impl RGB {
  pub fn from_hex_string(str: &str) -> Result<Self, hex::FromHexError> {
    let mut hex_color = [0u8; 3];
    hex::decode_to_slice(str, &mut hex_color)?;
    Ok(Self {
      r: hex_color[0],
      g: hex_color[1],
      b: hex_color[2],
    })
  }
}

pub struct Ws2812<'d, P: Instance, const S: usize> {
  sm: StateMachine<'d, P, S>,
}

impl<'d, P: Instance, const S: usize> Ws2812<'d, P, S> {
  pub fn new(pin: impl PioPin, pio: &mut Common<'d, P>, mut sm: StateMachine<'d, P, S>) -> Self {
    let prg = pio_proc::pio_asm!(
      "
      .define START_BIT_DELAY 1
      .define DATA_BIT_DELAY 4
      .define STOP_BIT_DELAY 2

      .side_set 1

      set pindirs, 1    side 0

      .wrap_target
      bitloop:
      out x, 1        side 0 [STOP_BIT_DELAY]
      jmp !x do_zero  side 1 [START_BIT_DELAY]

      jmp bitloop     side 1 [DATA_BIT_DELAY]

      do_zero:
        nop           side 0 [DATA_BIT_DELAY]
      .wrap
      "
    );

    sm.set_enable(false);

    let mut cfg = Config::default();

    let out_pin = pio.make_pio_pin(pin);
    cfg.set_out_pins(&[&out_pin]);
    cfg.set_set_pins(&[&out_pin]);

    cfg.use_program(&pio.load_program(&prg.program), &[&out_pin]);

    let clock_freq = U24F8::from_num(clocks::clk_sys_freq() / 1000);
    let ws2812_freq = fixed!(800: U24F8);
    let bit_freq = ws2812_freq * 10;
    cfg.clock_divider = clock_freq / bit_freq;

    // let pio::Wrap { source, target } = relocated.wrap();
    // sm.set_wrap(source, target);

    cfg.fifo_join = FifoJoin::TxOnly;
    cfg.shift_out = ShiftConfig {
      auto_fill: true,
      threshold: 24,
      direction: ShiftDirection::Left,
    };

    sm.set_config(&cfg);
    sm.set_enable(true);

    Self { sm }
  }

  pub async fn set_color(&mut self, color: &RGB) {
    self
      .sm
      .tx()
      .wait_push(((color.g as u32) << 24) + ((color.r as u32) << 16) + ((color.b as u32) << 8))
      .await;
  }
}
