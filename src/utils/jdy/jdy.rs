use core::fmt::Arguments;

use embassy_rp::uart::{Async, Instance, Uart, UartRx, UartTx};

use crate::helpers::write_to_buffer;

pub struct Jdy<T: Instance + 'static> {
  rx: UartRx<'static, T, Async>,
  tx: UartTx<'static, T, Async>,
  rx_buffer: [u8; 100],
  tx_buffer: [u8; 100],
}

impl<T: Instance> Jdy<T> {
  pub fn new(uart: Uart<'static, T, Async>) -> Self {
    let (tx, rx) = uart.split();
    Self {
      rx,
      tx,
      rx_buffer: [0u8; 100],
      tx_buffer: [0u8; 100],
    }
  }

  pub async fn wait_for_data(&mut self) -> Result<&str, ()> {
    let mut msg_idx = 0;
    loop {
      let mut buf = [0u8; 1];
      let status = self.rx.read(&mut buf).await;
      if status.is_err() {
        return Err(());
      }
      if buf[0] == 0 || (buf[0] == b'\n' && msg_idx == 0) {
        continue;
      }
      self.rx_buffer[msg_idx] = buf[0];
      msg_idx = (msg_idx + 1) % self.rx_buffer.len();
      if buf[0] == b'\n' {
        break;
      }
    }
    let str_data = core::str::from_utf8(&self.rx_buffer[0..(msg_idx - 2)]);
    match str_data {
      Ok(s) => {
        // info!("Got response: {}", s);
        Ok(s)
      }
      Err(_) => {
        // info!("Got response: <invalid utf8>");
        Err(())
      }
    }
  }

  pub async fn send_with_args<'a>(&mut self, data: Arguments<'a>) -> Result<(), ()> {
    // let mut debug_buffer: String<30> = String::new();
    // let _ = core::fmt::write(&mut debug_buffer, data);
    // debug_buffer.truncate(debug_buffer.len() - 2);
    // debug!("Sending: {}", debug_buffer);
    let written = write_to_buffer(&mut self.tx_buffer, data).unwrap();
    self.tx.write(&self.tx_buffer[0..written]).await.unwrap();
    Ok(())
  }
}
