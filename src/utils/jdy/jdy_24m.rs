use core::fmt::{Arguments, Write};

use embassy_rp::gpio::{AnyPin, Level, Output};
use embassy_rp::uart::{Async, Instance, Uart};
use embassy_time::{with_timeout, Duration, Timer};
use heapless::String;

use super::Jdy;

pub struct Jdy24mMaster<T: Instance + 'static> {
  jdy: Jdy<T>,
  control_pin: Output<'static, AnyPin>,
}

impl<T: Instance> Jdy24mMaster<T> {
  pub fn new(uart: Uart<'static, T, Async>, en_pin: AnyPin) -> Self {
    Self {
      jdy: Jdy::new(uart),
      control_pin: Output::new(en_pin, Level::High),
    }
  }

  pub async fn init(&mut self) -> Result<(), ()> {
    self.wake().await?;
    // self.send_instruction_assert_response(format_args!("BAUD8"), "OK", None).await?;
    self.send_instruction_assert_response(format_args!("ROLE1"), "OK", None).await?;
    self.send_instruction_assert_response(format_args!("RESET"), "OK", None).await?;
    self
      .wait_for_response(|v| v.starts_with("+JDY-24-START"), Duration::from_millis(2000))
      .await?;
    self.sleep().await?;
    Ok(())
  }

  pub async fn connect_and_send(&mut self, addr: &str, data: &str) -> Result<(), ()> {
    let _ = self.wake().await;
    let mut send_successful = false;
    let mut attempts_left = 100;
    while !send_successful && attempts_left > 0 {
      attempts_left -= 1;

      if self.connect(addr).await.is_ok() {
        if self.send_data(format_args!("{}", data)).await.is_ok() {
          // Disconnecting too quickly after sending data will cause the data to not be sent
          Timer::after(Duration::from_millis(2000)).await;
          send_successful = true;
        }
        let _ = self.disconnect().await;
      }
    }
    let _ = self.sleep().await;
    if send_successful {
      Ok(())
    } else {
      Err(())
    }
  }

  async fn connect(&mut self, addr: &str) -> Result<(), ()> {
    self.send_instruction(format_args!("CONA{}", addr)).await?;
    let connect_res = self
      .wait_for_response(
        |v| v.starts_with("+CONNECTED") | v.starts_with("+DISCONNECT") | v.starts_with("+CONNECTION TIME OUT"),
        Duration::from_secs(20),
      )
      .await;
    if !connect_res.is_ok_and(|v| v.starts_with("+CONNECTED")) {
      let _fail_reason = self.wait_for_response(|_| true, Duration::from_millis(100)).await;
      return Err(());
    }
    let status = self
      .wait_for_response(|v| v.starts_with("+DISCONNECT"), Duration::from_millis(2500))
      .await;
    if status.is_ok() {
      Err(())
    } else {
      Ok(())
    }
  }

  async fn disconnect(&mut self) -> Result<(), ()> {
    self.send_instruction(format_args!("STAT")).await?;
    let stat_res = self.wait_for_response(|v| v.starts_with("+STAT"), Duration::from_millis(200)).await?;
    if stat_res == "+STAT=1" {
      self.send_instruction_assert_response(format_args!("DISC"), "+DISCONNECT", None).await?;
    }
    Ok(())
  }

  async fn sleep(&mut self) -> Result<(), ()> {
    self.send_instruction_assert_response(format_args!("SLEEP2"), "+SLEEP", None).await?;
    self.control_pin.set_high();
    Ok(())
  }

  async fn wake(&mut self) -> Result<(), ()> {
    // This is to handle 2 +WAKE messages and already being awake
    let mut retries = 0;
    while retries < 2 {
      if self
        .send_instruction_assert_response(format_args!("VERSION"), "+VERSION", Some(Duration::from_millis(500)))
        .await
        .is_ok()
      {
        return Ok(());
      }
      retries += 1;
    }
    Err(())
  }

  async fn send_instruction_assert_response<'a>(
    &mut self,
    command: Arguments<'a>,
    response_prefix: &str,
    timeout: Option<Duration>,
  ) -> Result<(), ()> {
    self.send_instruction(command).await?;
    self
      .wait_for_response(|v| v.starts_with(response_prefix), timeout.unwrap_or(Duration::from_millis(200)))
      .await?;
    Ok(())
  }

  async fn send_instruction<'a>(&mut self, command: Arguments<'a>) -> Result<(), ()> {
    self.control_pin.set_low();
    self.jdy.send_with_args(format_args!("AT+{}\r\n", command)).await?;
    Ok(())
  }

  async fn send_data<'a>(&mut self, data: Arguments<'a>) -> Result<(), ()> {
    self.control_pin.set_high();
    self.jdy.send_with_args(format_args!("{}\r\n", data)).await
  }

  async fn wait_for_response<'a>(
    &mut self,
    response_filter: impl Fn(&str) -> bool + core::marker::Copy,
    timeout: Duration,
  ) -> Result<String<30>, ()> {
    with_timeout(timeout, self.wait_for_response_forever(response_filter))
      .await
      .map_err(|_| ())
  }

  async fn wait_for_response_forever(&mut self, response_filter: impl Fn(&str) -> bool + core::marker::Copy) -> String<30> {
    let mut res_buffer: String<30> = String::new();
    loop {
      let res = self.jdy.wait_for_data().await;
      if res.is_ok_and(response_filter) && res_buffer.write_str(res.unwrap()).is_ok() {
        return res_buffer;
      }
    }
  }
}
