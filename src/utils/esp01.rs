use core::fmt::Arguments;

use defmt::*;
use embassy_futures::select::{select, Either};
use embassy_rp::peripherals::UART1;
use embassy_rp::uart::{Async, Uart, UartRx, UartTx};
use embassy_time::{Duration, Timer};

use crate::helpers::write_to_buffer;

pub struct Esp01 {
  rx: UartRx<'static, UART1, Async>,
  tx: UartTx<'static, UART1, Async>,
  rx_buffer: [u8; 128],
  tx_buffer: [u8; 128],
}

impl Esp01 {
  pub fn new(uart: Uart<'static, UART1, Async>) -> Self {
    let (tx, rx) = uart.split();
    Self {
      rx,
      tx,
      rx_buffer: [0u8; 128],
      tx_buffer: [0u8; 128],
    }
  }

  async fn wait_for_data(&mut self) -> Result<&str, ()> {
    let mut msg_idx = 0;
    loop {
      let mut buf = [0u8; 1];
      let status = self.rx.read(&mut buf).await;
      if status.is_err() {
        return Err(());
      }
      if buf[0] == 0 || (buf[0] == b'\n' && msg_idx == 0) {
        continue;
      }

      self.rx_buffer[msg_idx] = buf[0];
      msg_idx = (msg_idx + 1) % self.rx_buffer.len();
      if buf[0] == b'\n' {
        break;
      }
    }
    let str_data = core::str::from_utf8(&self.rx_buffer[0..(msg_idx - 2)]);
    match str_data {
      Ok(s) => {
        debug!("Got response: '{}'", s);
        Ok(s)
      }
      Err(_) => {
        debug!("Got response: <invalid utf8>");
        Err(())
      }
    }
  }

  pub async fn drain_until_timeout(&mut self, timeout: Duration) {
    loop {
      let value = select(self.wait_for_data(), Timer::after(timeout)).await;
      if let Either::Second(_) = value {
        break;
      }
    }
  }

  pub async fn command<'a>(&mut self, command: Arguments<'a>, expected_results: &[&str]) -> Result<(), ()> {
    let written = write_to_buffer(&mut self.tx_buffer, format_args!("{}\r\n", command)).unwrap();
    debug!("Sending: {}", core::str::from_utf8(&self.tx_buffer[0..written]).unwrap().trim_end());
    self.tx.write(&self.tx_buffer[0..written]).await.unwrap();
    loop {
      let result = match select(self.wait_for_data(), Timer::after(Duration::from_secs(5))).await {
        Either::First(v) => v,
        Either::Second(_) => Err(()),
      }?;
      if expected_results.iter().any(|v| result.contains(v)) {
        break;
      }
    }
    Ok(())
  }

  pub async fn start_transparent_transmission(&mut self) -> Result<(), ()> {
    self.command(format_args!("AT+CIPMODE=1"), &["OK"]).await?;
    self.command(format_args!("AT+CIPSEND"), &["OK"]).await?;
    self.drain_until_timeout(Duration::from_millis(20)).await;
    Ok(())
  }
}

#[derive(Debug)]
pub struct CustomError(embassy_rp::uart::Error);
impl embedded_io::Error for CustomError {
  fn kind(&self) -> embedded_io::ErrorKind {
    embedded_io::ErrorKind::Other
  }
}

impl embedded_io::Io for Esp01 {
  type Error = CustomError;
}

impl embedded_io::asynch::Read for Esp01 {
  async fn read(&mut self, buf: &mut [u8]) -> Result<usize, Self::Error> {
    self.rx.read(buf).await.map_err(CustomError)?;
    Ok(buf.len())
  }
}

impl embedded_io::asynch::Write for Esp01 {
  async fn write(&mut self, buf: &[u8]) -> Result<usize, Self::Error> {
    self.tx.write(buf).await.map_err(CustomError)?;
    Ok(buf.len())
  }
}
