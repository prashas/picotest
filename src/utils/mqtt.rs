use defmt::*;
use embassy_executor::Spawner;
use embassy_futures::select::{select, Either};
use embassy_rp::peripherals::PIO0;
use embassy_rp::{gpio, interrupt, peripherals, pio};
use embassy_sync::blocking_mutex::raw::CriticalSectionRawMutex;
use embassy_sync::signal::Signal;
use embassy_time::{Duration, Timer};
use heapless::String;
use rust_mqtt::client::client::MqttClient;
use rust_mqtt::client::client_config::{ClientConfig, MqttVersion};
use rust_mqtt::packet::v5::reason_codes::ReasonCode;
use rust_mqtt::utils::rng_generator::CountingRng;
use static_cell::make_static;

#[embassy_executor::task]
async fn wifi_task(
  runner: cyw43::Runner<
    'static,
    gpio::Output<'static, peripherals::PIN_23>,
    cyw43_pio::PioSpi<'static, peripherals::PIN_25, peripherals::PIO0, 0, peripherals::DMA_CH0>,
  >,
) -> ! {
  runner.run().await
}

#[embassy_executor::task]
async fn net_task(stack: &'static embassy_net::Stack<cyw43::NetDriver<'static>>) -> ! {
  stack.run().await
}

#[embassy_executor::task]
async fn mqtt_task(signaller: &'static mut MqttSignaller) -> ! {
  signaller.run().await
}

type MqttRxTxSignalType = Signal<CriticalSectionRawMutex, (String<100>, String<500>)>;

struct MqttSignaller {
  device_name: &'static str,
  client: MqttClient<'static, &'static mut OldTcpSocket<'static>, 5, CountingRng>,
  rx_channel: &'static MqttRxTxSignalType,
  tx_channel: &'static MqttRxTxSignalType,
}

impl MqttSignaller {
  pub async fn run(&mut self) -> ! {
    let mut sub_topic: String<20> = String::new();
    core::fmt::write(&mut sub_topic, format_args!("{}/#", self.device_name)).unwrap();
    self.client.subscribe_to_topic(&sub_topic).await.unwrap();
    loop {
      let action = select(self.tx_channel.wait(), self.client.receive_message()).await;

      match action {
        Either::First((topic, payload)) => {
          info!("Got signal to send data.");
          let _ = self
            .client
            .send_message(
              &topic,
              payload.as_bytes(),
              rust_mqtt::packet::v5::publish_packet::QualityOfService::QoS1,
              false,
            )
            .await;
        }
        Either::Second(rec_opt) => match rec_opt {
          Ok((topic, payload)) => {
            if let Ok(payload_str) = core::str::from_utf8(payload) {
              info!("Got message on {}: {}", topic, payload_str);
              self.rx_channel.signal((String::from(topic), String::from(payload_str)));
            } else {
              info!("Got message on topic {} with invalid utf8 payload", topic);
            }
          }
          Err(ReasonCode::ImplementationSpecificError) => {}
          Err(e) => {
            warn!("Failed to receive message: {:?}", e);
          }
        },
      }
    }
  }
}

pub struct WifiResources {
  pub pwr: peripherals::PIN_23,
  pub dio: peripherals::PIN_24,
  pub cs: peripherals::PIN_25,
  pub clk: peripherals::PIN_29,
  pub pio: peripherals::PIO0,
  pub dma: peripherals::DMA_CH0,
}

pub async fn on_pico_w(
  irqs: impl interrupt::typelevel::Binding<interrupt::typelevel::PIO0_IRQ_0, pio::InterruptHandler<PIO0>>,
  wifi_resources: WifiResources,
  spawner: &Spawner,
  device_name: &'static str,
) -> (&'static MqttRxTxSignalType, &'static MqttRxTxSignalType) {
  let fw = unsafe { core::slice::from_raw_parts(0x10100000 as *const u8, 224190) };
  let clm = unsafe { core::slice::from_raw_parts(0x10140000 as *const u8, 4752) };

  let mut pwr = gpio::Output::new(wifi_resources.pwr, gpio::Level::Low);

  pwr.set_low();

  let cs = gpio::Output::new(wifi_resources.cs, gpio::Level::High);

  let mut pio = pio::Pio::new(wifi_resources.pio, irqs);
  let spi = cyw43_pio::PioSpi::new(
    &mut pio.common,
    pio.sm0,
    pio.irq0,
    cs,
    wifi_resources.dio,
    wifi_resources.clk,
    wifi_resources.dma,
  );

  let state = make_static!(cyw43::State::new());
  let (net_device, mut control, runner) = cyw43::new(state, pwr, spi, fw).await;

  unwrap!(spawner.spawn(wifi_task(runner)));

  control.init(clm).await;
  // control.set_power_management(cyw43::PowerManagementMode::None).await;

  let config = embassy_net::Config::dhcpv4(Default::default());

  let stack = make_static!(embassy_net::Stack::new(
    net_device,
    config,
    make_static!(embassy_net::StackResources::<2>::new()),
    0x0123_4567_89ab_cdef
  ));

  unwrap!(spawner.spawn(net_task(stack)));

  info!("===================================================== WIFI TIME!!! ================================================");

  loop {
    match control.join_wpa2(env!("WIFI_SSID"), env!("WIFI_PSK")).await {
      Ok(_) => break,
      Err(err) => {
        info!("join failed with status={}", err.status);
      }
    }
    Timer::after(Duration::from_secs(2)).await;
  }

  info!("===================================================== SOCKET TIME!!! ================================================");

  let tcp_rx_buffer = make_static!([0; 1024]);
  let tcp_tx_buffer = make_static!([0; 1024]);

  let mut socket = embassy_net::tcp::TcpSocket::new(stack, tcp_rx_buffer, tcp_tx_buffer);
  socket.set_keep_alive(None);
  loop {
    match socket.connect((embassy_net::Ipv4Address([192, 168, 1, 4]), 1883)).await {
      Ok(_) => break,
      Err(err) => {
        info!("socket failed with error {:?}", err);
      }
    }
    Timer::after(Duration::from_secs(2)).await;
  }

  let wrapped_socket = make_static!(OldTcpSocket(socket));

  let mut config = ClientConfig::new(MqttVersion::MQTTv5, CountingRng(20000));
  config.add_max_subscribe_qos(rust_mqtt::packet::v5::publish_packet::QualityOfService::QoS1);
  config.add_client_id(device_name);
  config.keep_alive = 0;

  let mqtt_rx_buffer = make_static!([0; 80]);
  let mqtt_tx_buffer = make_static!([0; 80]);
  let mut client = MqttClient::<_, 5, _>::new(wrapped_socket, mqtt_tx_buffer, 80, mqtt_rx_buffer, 80, config);

  info!("===================================================== MQTT TIME!!! ================================================");
  loop {
    match client.connect_to_broker().await {
      Ok(_) => break,
      Err(err) => {
        info!("mqtt broker connect error {:?}", err);
      }
    }
    Timer::after(Duration::from_secs(2)).await;
  }

  control.gpio_set(0, true).await;

  let rx_channel: &MqttRxTxSignalType = make_static!(Signal::new());
  let tx_channel: &MqttRxTxSignalType = make_static!(Signal::new());

  let signaller = make_static!(MqttSignaller {
    device_name,
    client,
    rx_channel,
    tx_channel,
  });
  unwrap!(spawner.spawn(mqtt_task(signaller)));

  (rx_channel, tx_channel)
}

struct OldTcpSocket<'a>(embassy_net::tcp::TcpSocket<'a>);

#[derive(Debug)]
struct CustomError(embassy_net::tcp::Error);
impl embedded_io::Error for CustomError {
  fn kind(&self) -> embedded_io::ErrorKind {
    embedded_io::ErrorKind::Other
  }
}

impl<'a> embedded_io::Io for OldTcpSocket<'a> {
  type Error = CustomError;
}
impl<'a> embedded_io::asynch::Read for OldTcpSocket<'a> {
  async fn read(&mut self, buf: &mut [u8]) -> Result<usize, CustomError> {
    match self.0.read(buf).await {
      Ok(v) => Ok(v),
      Err(e) => {
        error!("Failed to read from socket: {:?}", e);
        Err(CustomError(e))
      }
    }
  }
}

impl<'a> embedded_io::asynch::Write for OldTcpSocket<'a> {
  async fn write(&mut self, buf: &[u8]) -> Result<usize, CustomError> {
    self.0.write(buf).await.map_err(CustomError)
  }
}
