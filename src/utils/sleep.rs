use core::ptr::write_volatile;

use defmt::*;
use embassy_rp::gpio::InterruptTrigger;
use embassy_rp::pac;

pub enum DormantSource {
  None,
  ROSC,
  XOSC,
}

fn go_dormant(source: DormantSource) {
  info!("Going dormant");
  const DORMANT_VALUE: u32 = 0x636f6d61;
  match source {
    DormantSource::XOSC => {
      pac::XOSC.dormant().write_value(DORMANT_VALUE);
    }
    DormantSource::ROSC => {
      pac::ROSC.dormant().write_value(DORMANT_VALUE);
    }
    DormantSource::None => {}
  };
  info!("After dormant");
}

pub fn go_dormant_until_pin(pin: usize, level: InterruptTrigger, source: DormantSource) {
  let pin_group = pin % 8;
  pac::IO_BANK0.int_dormant_wake().inte(pin / 8).modify(|w| match level {
    InterruptTrigger::LevelHigh => {
      w.set_level_high(pin_group, true);
    }
    InterruptTrigger::LevelLow => {
      w.set_level_low(pin_group, true);
    }
    InterruptTrigger::EdgeHigh => {
      w.set_edge_high(pin_group, true);
    }
    InterruptTrigger::EdgeLow => {
      w.set_edge_low(pin_group, true);
    }
    InterruptTrigger::AnyEdge => {}
  });
  go_dormant(source)
}

pub unsafe fn software_reset() {
  // The soft reboot does not seem to restart the ROSC which is required
  pac::ROSC.ctrl().modify(|w| {
    w.set_enable(pac::rosc::vals::Enable::ENABLE);
  });

  // Ref: https://forums.raspberrypi.com/viewtopic.php?t=318747#p1928868
  const RESET_REGISTER: *mut u32 = (0xe0000000_u32 + 0x0ED0C_u32) as *mut u32;
  write_volatile(RESET_REGISTER, 0x5FA0004);
}
