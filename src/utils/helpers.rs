use core::cmp::min;
use core::fmt;

use embassy_rp::gpio::{AnyPin, Output};
use embassy_time::{Duration, Timer};

pub async fn blink_times(pin: &mut Output<'_, AnyPin>, times: u32, delay_override: Option<u64>) {
  let delay_duration = Duration::from_millis(delay_override.unwrap_or(200));
  for _ in 0..times {
    pin.set_high();
    Timer::after(delay_duration).await;
    pin.set_low();
    Timer::after(delay_duration).await;
  }
}

pub fn did_watchdog_cause_reboot() -> bool {
  let reg = embassy_rp::pac::WATCHDOG.reason().read();
  reg.force() || reg.timer()
}

struct WriteTo<'a> {
  pub buffer: &'a mut [u8],
  pub used: usize,
}

impl<'a> fmt::Write for WriteTo<'a> {
  fn write_str(&mut self, s: &str) -> fmt::Result {
    if self.used > self.buffer.len() {
      return Err(fmt::Error);
    }
    let remaining_buf = &mut self.buffer[self.used..];
    let raw_s = s.as_bytes();
    let write_num = min(raw_s.len(), remaining_buf.len());
    remaining_buf[..write_num].copy_from_slice(&raw_s[..write_num]);
    self.used += raw_s.len();
    if write_num < raw_s.len() {
      Err(fmt::Error)
    } else {
      Ok(())
    }
  }
}

pub fn write_to_buffer(buffer: &mut [u8], args: fmt::Arguments<'_>) -> Result<usize, fmt::Error> {
  let mut writer = WriteTo { buffer, used: 0 };
  fmt::write(&mut writer, args)?;
  Ok(writer.used)
}
