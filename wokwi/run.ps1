$bin = $args[0];
echo "Building $bin"

cargo build --release --bin $bin
elf2uf2-rs $PSScriptRoot/../target/thumbv6m-none-eabi/release/$bin
[IO.File]::WriteAllLines("$PSScriptRoot/wokwi.toml", @"
[wokwi]
version = 1
firmware = "../target/thumbv6m-none-eabi/release/$bin.uf2"
elf = "../target/thumbv6m-none-eabi/release/$bin"
"@);
cp $PSScriptRoot/diagrams/$bin.json $PSScriptRoot/diagram.json
